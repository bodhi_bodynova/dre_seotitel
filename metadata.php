<?php
/**
 * Metadata version
 */
$sMetadataVersion = '2.0';

/**
 * Module information
 */
$aModule = array(
    'id'           => 'dre_seotitel',
    'title'        => '<img src="../modules/bender/dre_seotitel/out/img/favicon.ico" > SEO Title',
    'description'  => array(
        'de' => 'Mit diesem Modul können Sie den Meta Title von Artikeln, Kategorien, CMS-Seiten, Hersteller und Lieferanten manuell pflegen.'
    ),
    'thumbnail'    => 'out/img/logo_bodynova.png',
    'version'      => '0.0.3',
    'author'       => 'André Bender',
    'email'        => 'a.bender@bodynova.de',
    'url'          => 'https://bodynova.de',
    'extend'       => array(
        //'details'           => 'bender/dre_seotitle/Controller/dre_seotitle__details',
        \OxidEsales\Eshop\Application\Controller\ArticleDetailsController::class =>
            \Bender\SeoTitel\Controller\ArticleDetailsController::class,
        \OxidEsales\Eshop\Application\Controller\ArticleListController::class =>
            \Bender\SeoTitel\Controller\ArticleListController::class,
        \OxidEsales\Eshop\Application\Controller\ContentController::class =>
            \Bender\SeoTitel\Controller\ContentController::class,
        \OxidEsales\Eshop\Application\Controller\ManufacturerListController::class =>
            \Bender\SeoTitel\Controller\ManufacturerListController::class,
        \OxidEsales\Eshop\Application\Controller\VendorListController::class =>
            \Bender\SeoTitel\Controller\VendorListController::class
    ),
    'controllers' => array(
    ),
    'events'       => array(
        'onActivate'   => '\Bender\SeoTitel\Core\Events::onActivate',
        'onDeactivate' => '\Bender\SeoTitel\Core\Events::onDeactivate'
    ),
    'templates' => array(
    ),
    'blocks' => array(
        array('template' => 'article_main.tpl', 'block'=>'admin_article_main_form', 'file'=>'views/blocks/admin_article_main_form.tpl'),
        array('template' => 'include/category_main_form.tpl', 'block'=>'admin_category_main_form', 'file'=>'views/blocks/admin_category_main_form.tpl'),
        array('template' => 'content_main.tpl', 'block'=>'admin_content_main_form', 'file'=>'views/blocks/admin_content_main_form.tpl'),
        array('template' => 'manufacturer_main.tpl', 'block'=>'admin_manufacturer_main_form', 'file'=>'views/blocks/admin_manufacturer_main_form.tpl'),
        array('template' => 'vendor_main.tpl', 'block'=>'admin_vendor_main_form', 'file'=>'views/blocks/admin_vendor_main_form.tpl'),
     ),
    'settings' => array(
    )
);
