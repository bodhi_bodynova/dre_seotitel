<?php
namespace Bender\SeoTitel\Controller;

class ArticleListController extends ArticleListController_parent {

    public function getSeoTitle() {
        if ($oCategory = $this->getActCategory()) {
            $sSeoTitle = $oCategory->oxcategories__dre_seotitle->value;
            if (empty($sSeoTitle)) {
                // check field with sql because lazy loading is maybe activated
                $sSeoTitle = $this->_getSeoTitleFromDb($oCategory->oxcategories__oxid->value);
            }
            return $sSeoTitle;
        }
    }

    public function getPageH1Title()
    {
        if ($oCategory = $this->getActCategory()) {
            $sSeoH = $oCategory->oxcategories__dre_seoh->value;
            if (empty($sSeoH)) {
                // check field with sql because lazy loading is maybe activated
                $sSeoH = $this->_getSeoHTitleFromDb($oCategory->oxcategories__oxid->value);
            }
            return $sSeoH;
        }
    }


    protected function _getSeoTitleFromDb($sOxid, $sField = 'dre_seotitle') {
        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb();
        $sView = getViewName('oxcategories');
        $sSelect = "Select $sField from $sView where oxid = '$sOxid'";
        $sResult = $oDb->getOne($sSelect);
        return $sResult;
    }

    protected function _getSeoHTitleFromDb($sOxid, $sField = 'dre_seoh'){
        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb();
        $sView = getViewName('oxcategories');
        $sSelect = "Select $sField from $sView where oxid = '$sOxid'";
        $sResult = $oDb->getOne($sSelect);
        return $sResult;
    }
    
    /**
     * Returns full page title
     *
     * @return string
     */
    public function getPageTitle()
    {
        $sSeoTitle = $this->getSeoTitle();
        if(empty($sSeoTitle)){
            //no seo title is set, so process the parent call
            return parent::getPageTitle();
        }
        
        $sTitle = '';

        $aTitleParts = array();
        $aTitleParts[] = $this->getTitlePrefix();
        $aTitleParts[] = $sSeoTitle;
        $aTitleParts[] = $this->getTitleSuffix();
        $aTitleParts[] = $this->getTitlePageSuffix();

        $aTitleParts = array_filter($aTitleParts);

        if (count($aTitleParts)) {
            $sTitle = implode(' | ', $aTitleParts);
        }


        return $sTitle;
    }

    public function getSeoH1(){
        $sSeoH1 = $this->getPageH1Title();
        if (empty($sSeoH1)) {
            $oCategory = $this->getActCategory();
            return $oCategory->oxcategories__oxtitle->value;
        }
        return $sSeoH1;
    }


}
