<?php
namespace Bender\SeoTitel\Controller;

class ContentController extends ContentController_parent {

    /**
     * @return false|\OxidEsales\EshopCommunity\Application\Controller\sting|string
     */
    public function getTitle() {
        if ( $oContent = $this->getContent() ) {
            $sSeoTitle = $oContent->oxcontents__dre_seotitle->value;
            if (empty($sSeoTitle)) {
                // check field with sql because lazy loading is maybe activated
                $sSeoTitle = $this->_getSeoTitleFromDb($oContent->oxcontents__oxid->value);
            }
            if (!empty($sSeoTitle)) {
                return $sSeoTitle;
            } else {
                return parent::getTitle();
            }
        }
    }

    /**
     * @param $sOxid
     * @param string $sField
     * @return false|string
     * @throws \OxidEsales\Eshop\Core\Exception\DatabaseConnectionException
     */
    protected function _getSeoTitleFromDb($sOxid, $sField = 'dre_seotitle')
    {
        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb();
        $sView = getViewName('oxcontents');
        $sSelect = "Select $sField from $sView where oxid = '$sOxid'";
        $sResult = $oDb->getOne($sSelect);
        return $sResult;
    }

    /**
     * @return false|\OxidEsales\EshopCommunity\Application\Controller\sting|string
     */
    public function getSeoH()
    {
        if ($oContent = $this->getContent()) {
            $sSeoH = $oContent->oxcontents__dre_seoh->value;
            if (empty($sSeoH)) {
                // check field with sql because lazy loading is maybe activated
                $sSeoH = $this->_getSeoHFromDb($oContent->oxcontents__oxid->value);
            }
            if (!empty($sSeoH)) {
                return $sSeoH;
            } else {
                return parent::getTitle();
            }
        }
    }

    /**
     * @param $sOxid
     * @param string $sField
     * @return false|string
     * @throws \OxidEsales\Eshop\Core\Exception\DatabaseConnectionException
     */
    protected function _getSeoHFromDb($sOxid, $sField = 'dre_seoh')
    {
        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb();
        $sView = getViewName('oxcontents');
        $sSelect = "Select $sField from $sView where oxid = '$sOxid'";
        $sResult = $oDb->getOne($sSelect);
        return $sResult;
    }

    /**
     * @return \OxidEsales\EshopCommunity\Application\Controller\sting|string
     */
    public function getContentTitle(){
        if ($oContent = $this->getContent()) {
            $sTitle = $oContent->oxcontents__oxtitle->value;
            if (empty($sTitle)) {
                // check field with sql because lazy loading is maybe activated
                $sSeoTitle = $this->_getContentTitleFromDb($oContent->oxcontents__oxid->value);
            }
            if (!empty($sTitle)) {
                return $sTitle;
            } else {
                return parent::getTitle();
            }
        }
    }

    /**
     * @param $sOxid
     * @param string $sField
     * @return false|string
     * @throws \OxidEsales\Eshop\Core\Exception\DatabaseConnectionException
     */
    protected function _getContentTitleFromDb($sOxid, $sField = 'oxtitle')
    {
        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb();
        $sView = getViewName('oxcontents');
        $sSelect = "Select $sField from $sView where oxid = '$sOxid'";
        $sResult = $oDb->getOne($sSelect);
        return $sResult;
    }

}
