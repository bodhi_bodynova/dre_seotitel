<?php
namespace Bender\SeoTitel\Controller;

class ArticleDetailsController extends ArticleDetailsController_parent {

    public function getTitle() {
        if ( $oProduct = $this->getProduct() ) {
            $sSeoTitle = $oProduct->oxarticles__dre_seotitle->value;
            if($sSeoTitle && $sSeoTitle != ''){
                return $sSeoTitle;
            } else {
                return parent::getTitle();
            }
        }
    }

    public function getSeoH1(){
        if ($oProduct = $this->getProduct()) {
            $sSeoTitle = $oProduct->oxarticles__dre_seoh->value;
            if ($sSeoTitle && $sSeoTitle != '') {
                return $sSeoTitle;
            } else {
                return $oProduct->oxarticles__oxtitle->value;
            }
        }
    }

}
