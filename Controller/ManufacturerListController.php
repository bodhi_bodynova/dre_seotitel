<?php
namespace Bender\SeoTitel\Controller;

class ManufacturerListController extends ManufacturerListController_parent {

    public function getTitle() {
        if ($oManufacturer = $this->getActManufacturer()) {
            $sSeoTitle = $oManufacturer->oxmanufacturers__dre_seotitle->value;
            if (empty($sSeoTitle)) {
                // check field with sql because lazy loading is maybe activated
                $sSeoTitle = $this->_getSeoTitleFromDb($oManufacturer->oxmanufacturers__oxid->value);
            }
            if (!empty($sSeoTitle)) {
                return $sSeoTitle;
            } else {
                return parent::getTitle();
            }
        }
    }

    protected function _getSeoTitleFromDb($sOxid, $sField = 'dre_seotitle') {
        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb();
        $sView = getViewName('oxmanufacturers');
        $sSelect = "Select $sField from $sView where oxid = '$sOxid'";
        $sResult = $oDb->getOne($sSelect);
        return $sResult;
    }

    protected function _getSeoHTitleFromDb($sOxid, $sField = 'dre_seoh')
    {
        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb();
        $sView = getViewName('oxmanufacturers');
        $sSelect = "Select $sField from $sView where oxid = '$sOxid'";
        $sResult = $oDb->getOne($sSelect);
        return $sResult;
    }

    public function getSeoH1(){
        $sSeoH1 = $this->getPageH1Title();
        if (empty($sSeoH1)) {
            $oManufacturer = $this->getActManufacturer();
            return $oManufacturer->oxmanufacturers__oxtitle->value;
        }
        return $sSeoH1;
    }

    public function getPageH1Title()
    {
        if ($oManufacturer = $this->getActManufacturer()) {
            $sSeoH = $oManufacturer->oxmanufacturers__dre_seoh->value;
            if (empty($sSeoH)) {
                // check field with sql because lazy loading is maybe activated
                $sSeoH = $this->_getSeoHTitleFromDb($this->oxmanufacturers__oxid->value);
            }
            if (!empty($sSeoH)) {
                return $sSeoH;
            } else {
                return parent::getTitle();
            }
        }
    }

}
