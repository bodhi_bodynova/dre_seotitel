<tr>
  <td class="edittext">
    [{ oxmultilang ident="SEO_TITLE" }]&nbsp;
  </td>
  <td class="edittext">
    <input type="text" class="editinput" size="32" maxlength="[{$edit->oxcategories__dre_seotitle->fldmax_length}]" name="editval[oxcategories__dre_seotitle]" value="[{$edit->oxcategories__dre_seotitle->value}]" [{$readonly}]>
    [{ oxinputhelp ident="HELP_SEO_TITLE" }]
  </td>
</tr>
<tr>
  <td class="edittext">
    [{ oxmultilang ident="SEO_H1" }]&nbsp;
  </td>
  <td class="edittext">
    <input type="text" class="editinput" size="32" maxlength="[{$edit->oxcategories__dre_seoh->fldmax_length}]"name="editval[oxcategories__dre_seoh]" value="[{$edit->oxcategories__dre_seoh->value}]"[{$readonly}]>
    [{ oxinputhelp ident="HELP_SEO_TITLE" }]
  </td>
</tr>
[{$smarty.block.parent}]