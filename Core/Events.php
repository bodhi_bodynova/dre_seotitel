<?php
namespace Bender\SeoTitel\Core;

class Events {

    /**
     * Execute action on activate event
     */
    public static function onActivate()
    {
        self::addTableFields();
        
        $oDbHandler = oxNew( 'oxDbMetaDataHandler' );
        $oDbHandler->updateViews();
        
        self::clearTmp();
    }

    /**
     * Execute action on deactivate event
     *
     * @return null
     */
    public static function onDeactivate()
    {
        self::clearTmp();
    }


    /**
     * Add new fields
     */
    public static function addTableFields()
    {

        $dbMetaDataHandler = oxNew(\OxidEsales\Eshop\Core\DbMetaDataHandler::class);

        $tableFields = array(
            'oxarticles'        => array('DRE_SEOTITLE', 'DRE_SEOTITLE_1', 'DRE_SEOTITLE_2', 'DRE_SEOH', 'DRE_SEOH_1', 'DRE_SEOH_2'),
            'oxcategories'      => array('DRE_SEOTITLE', 'DRE_SEOTITLE_1', 'DRE_SEOTITLE_2', 'DRE_SEOH', 'DRE_SEOH_1', 'DRE_SEOH_2'),
            'oxcontents'        => array('DRE_SEOTITLE', 'DRE_SEOTITLE_1', 'DRE_SEOTITLE_2', 'DRE_SEOH', 'DRE_SEOH_1', 'DRE_SEOH_2'),
            'oxmanufacturers'   => array('DRE_SEOTITLE', 'DRE_SEOTITLE_1', 'DRE_SEOTITLE_2', 'DRE_SEOH', 'DRE_SEOH_1', 'DRE_SEOH_2'),
            'oxvendor'          => array('DRE_SEOTITLE', 'DRE_SEOTITLE_1', 'DRE_SEOTITLE_2', 'DRE_SEOH', 'DRE_SEOH_1', 'DRE_SEOH_2'),
        );

        foreach ($tableFields as $tableName => $fieldNames) {
            foreach($fieldNames as $fieldName){
                if (!$dbMetaDataHandler->fieldExists($fieldName, $tableName)) {
                    \OxidEsales\Eshop\Core\DatabaseProvider::getDb()->execute(
                        "ALTER TABLE `" . $tableName
                        . "` ADD `" . $fieldName . "` VARCHAR( 255 ) NOT NULL DEFAULT '';"
                    );
                }
            }
        }
    }
    
    public static function clearTmp( $sClearFolderPath = '' )
    {
        $sTempFolderPath = realpath(\OxidEsales\Eshop\Core\Registry::getConfig()->getConfigParam( 'sCompileDir' ));

        if ( !empty( $sClearFolderPath ) and
             ( strpos( $sClearFolderPath, $sTempFolderPath ) !== false ) and
             is_dir( $sClearFolderPath )
        ) {

            // User argument folder path to delete from
            $sFolderPath = $sClearFolderPath;
        } elseif ( empty( $sClearFolderPath ) ) {

            // Use temp folder path from settings
            $sFolderPath = $sTempFolderPath;
        } else {
            return false;
        }

        $hDir = opendir( $sFolderPath );

        if ( !empty( $hDir ) ) {
            while ( false !== ( $sFileName = readdir( $hDir ) ) ) {
                $sFilePath = $sFolderPath . '/' . $sFileName;

                if ( !in_array( $sFileName, array('.', '..', '.htaccess') ) and is_file( $sFilePath ) ) {

                    // Delete a file if it is allowed to delete
                    @unlink( $sFilePath );
                } elseif ( $sFileName == 'smarty' and is_dir( $sFilePath ) ) {

                    // Recursive call to clear smarty folder
                    self::clearTmp( $sFilePath );
                }
            }
        }

        return true;
    }

}
